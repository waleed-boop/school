<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;


class AdminAuth  {
	/**
	* Handle an incoming request.
	*
	* @param  \Illuminate\Http\Request  $request
	* @param  \Closure  $next
	* @return mixed
	*/
	//Replace handle function:
    public function handle($request, Closure $next) {
        //The following line(s) will be specific to your project, and depend on whatever you need as an authentication.
          $isAuthenticatedAdmin = (Auth::check() && Auth::user()->admin == 0);

          //This will be excecuted if the new authentication fails.
        if (! $isAuthenticatedAdmin)
            return redirect('/login')->with('message', 'Authentication Error.');
        return $next($request);
    }
	}


