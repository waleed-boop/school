<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\StoreUser;
use App\Http\Requests\registrationStore;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use flush;

class CustomAuthController extends Controller
{

    public function index()
    {
        return view('auth.login');
    }


    public function customLogin( StoreUser $request)
    {


        $credentials = $request->only('email', 'password');

        $user = User::where('email', $request->email)->first();

        if ($user->is_admin == 1)

        {
            Auth::attempt($credentials);
            return redirect()->route('home1');
        }
         else {

            Session::flash('error', 'Oopes! You have no permission ');
            return back();
        }



        // if (Auth::attempt($credentials)) {
        //   $user = User::where('email', $request->email)->first();

        //   if ($user->is_admin == 1)

        //   {
        //       return redirect()->route('home1',);
        //   } else {
        //       return redirect()->back()->with('error', 'Oppes! You have no permission ');
        //   }
        // }
    }



    public function registration()
    {
        return view('auth.registration');
    }


    public function customRegistration(registrationStore $request)
    {
        $request->validate([

        ]);

        $data = $request->all();
        $check = $this->create($data);

        return redirect("/");
    }


    public function create(array $data)
    {
      return User::create([
        'name' => $data['name'],
        'email' => $data['email'],
        'is_admin' => $data['is_admin'],
        'password' => Hash::make($data['password'])
      ]);
    }




    public function signout()
     {
        Auth::logout();
        return redirect('/login');
    }
}
