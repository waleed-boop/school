<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class course extends Model
{

    use HasFactory;
    protected $fillable = [
        'id',
        'name',
        'student_id',
    ];

//protected $table=
//"courses"
//;

    public function students()
    {
        return $this->belongsToMany(Student::class,'student_course','student_id','course_id');
    }
}
