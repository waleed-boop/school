<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('users')->delete(); //for cleaning earlier data to avoid duplicate entries
        DB::table('users')->insert([


           'name' => 'waleed',
          'email' => 'waleedramzan001@gmail.com',
          'role' => 'admin',
          'password' => Hash::make('password'),
        ]);
      }
}

